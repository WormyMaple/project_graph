using Godot;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.IO;
using FileAccess = Godot.FileAccess;

public partial class GraphSave : Node2D
{
    public class GraphMetaData
    {
        public GraphNodeInfo[] graphNodes { get; set; }

        public GraphMetaData(GraphNodeInfo[] inNodes) => graphNodes = inNodes;
        
        [JsonConstructor]
        public GraphMetaData() { }
    }
    
    public class GraphNodeInfo
    {
        public string label { get; set; }
        public string description { get; set; }
        public int index { get; set; }
        public List<int> connectedIndices { get; set; }
        public float[] position { get; set; }

        public GraphNodeInfo(GraphNode inNode, int inIndex)
        {
            string[] info = inNode.GetInfo();
            label = info[0];
            description = info[1];
            index = inIndex;
            position = new[] { inNode.GlobalPosition.X, inNode.GlobalPosition.Y };

            connectedIndices = new List<int>();
        }

        [JsonConstructor]
        public GraphNodeInfo() { }
    }
    
    [Export] GraphNode rootNode;
    [Export] CameraController mainCamera;
    bool firstFrame = true;

    public override void _Process(double delta)
    {
        if (firstFrame)
        {
            LoadGraph();
            firstFrame = false;
        }
    }

    void SaveGraph()
    {
        List<GraphNode> allGraphNodes = GetGraphNodes(rootNode);
        GraphNodeInfo[] newNodeInfos = new GraphNodeInfo[allGraphNodes.Count];
        
        for (int i = 0; i < allGraphNodes.Count; i += 1)
        {
            GraphNodeInfo newInfo = new GraphNodeInfo(allGraphNodes[i], i);
            for (int j = 0; j < allGraphNodes.Count; j += 1)
            {
                if (j == i) continue;

                if (allGraphNodes[i].outputNodes.Contains(allGraphNodes[j]))
                {
                    newInfo.connectedIndices.Add(j);
                }
            }

            newNodeInfos[i] = newInfo;
        }

        GraphMetaData newGraphData = new GraphMetaData(newNodeInfos);

        string newJson = JsonSerializer.Serialize(newGraphData, new JsonSerializerOptions { WriteIndented = true });
        
        FileAccess graphFile = FileAccess.Open("user://sample_graph.json", FileAccess.ModeFlags.Write);
        graphFile.StoreString(newJson);
        graphFile.Close();
    }

    void LoadGraph()
    {
        if (!FileAccess.FileExists("user://sample_graph.json"))
        {
            GD.Print("Couldn't find suitable save file");
            return;
        }
        
        FileAccess graphFile = FileAccess.Open("user://sample_graph.json", FileAccess.ModeFlags.Read);
        string readFile = graphFile.GetAsText();

        GraphMetaData loadedGraphData = JsonSerializer.Deserialize<GraphMetaData>(readFile);
        GraphNode[] newNodes = new GraphNode[loadedGraphData.graphNodes.Length];

        for (int i = 0; i < newNodes.Length; i += 1)
        {
            GraphNodeInfo nodeInfo = loadedGraphData.graphNodes[i];
            Vector2 nodePos = new Vector2(nodeInfo.position[0], nodeInfo.position[1]);
            GraphNode spawnedNode = mainCamera.SpawnGraphNode(nodePos, nodeInfo.label, nodeInfo.description);

            newNodes[i] = spawnedNode;
        }

        for (int i = 0; i < newNodes.Length; i += 1)
        {
            foreach (int outputNode in loadedGraphData.graphNodes[i].connectedIndices)
            {
                newNodes[i].ConnectGraphNode(newNodes[outputNode], true);
            }
        }
        
        rootNode.QueueFree();
        rootNode = newNodes[0];
    }

    List<GraphNode> GetGraphNodes(GraphNode startNode)
    {
        if (startNode.outputNodes.Count == 0) return new List<GraphNode> { startNode };

        List<GraphNode> outputNodes = new (){ startNode };

        foreach (GraphNode outputNode in startNode.outputNodes)
        {
            outputNodes.AddRange(GetGraphNodes(outputNode));
        }

        return outputNodes;
    }
}
