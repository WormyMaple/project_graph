using Godot;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

public partial class GraphNode : Node2D
{
	[Export] public CameraController mainCamera;
	[Export] public Sprite2D nodeOutput;
	[Export] public Sprite2D nodeInput;

	[Export] float bezierControlLength;
	[Export] int bezierSteps;
	[Export] PackedScene lineScene;

	[Export] RichTextLabel mainLabel, descLabel;
	[Export] Control descriptionParent;
	[Export] Area2D descEditArea, descArea;
	[Export] Sprite2D dropdown;

	bool draggingOutput;
	bool[] interactBuffer = new bool[6]; // Output, Input, Header, Dropdown, Desc. Edit, Label Edit
	
	public List<GraphNode> outputNodes = new();
	List<Line2D> outputLines = new();
	Line2D currentOutputLine;
	
	GraphNode inputNode;
	
	bool draggingNode;
	Vector2 nodeOffset;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		if (interactBuffer[1]) // Input grabbable
		{
			mainCamera.targetInput = this;
		}
		else if (mainCamera.targetInput == this) mainCamera.targetInput = null;
		
		if (Input.IsActionJustPressed("mouse_down")) // Output dragging check
		{
			for (int i = 0; i < interactBuffer.Length; i += 1)
			{
				if (!interactBuffer[i]) continue;
				
				// Interactable switch
				switch (i) // 
				{
					case 0:
						StartDrag();
						break;
					case 2:
						draggingNode = true;
						mainCamera.overrideDragging = true;
						break;
					case 3:
						ToggleDescription();
						break;
					case 4:
						mainCamera.EditDescription(this, descLabel.Text);
						break;
					case 5:
						EditLabel();
						break;
				}
			}

			nodeOffset = GlobalPosition - mainCamera.GetGlobalMousePosition();
		}

		if (draggingOutput || draggingNode)
		{
			if (Input.IsActionJustReleased("mouse_down"))
			{
				if (draggingOutput)
				{
					if (mainCamera.targetInput != null && mainCamera.targetInput != this) ConnectGraphNode(mainCamera.targetInput);
					else
					{
						currentOutputLine.QueueFree();
					}
				}

				draggingOutput = false;
				draggingNode = false;
				mainCamera.overrideDragging = false;
			}
		}

		if (draggingOutput) DragOutput();
		if (draggingNode) DragNode();
	}

	void StartDrag()
	{
		draggingOutput = true;
		mainCamera.overrideDragging = true;
		
		currentOutputLine = lineScene.Instantiate() as Line2D;
		AddChild(currentOutputLine);
	}

	void DragOutput()
	{
		Vector2 mousePos = mainCamera.GetGlobalMousePosition();
		
		UpdateBezier(currentOutputLine, nodeOutput.Position, mousePos - GlobalPosition);
	}

	void DragNode()
	{
		Vector2 mousePos = mainCamera.GetGlobalMousePosition();
		GlobalPosition = nodeOffset + mousePos;

		for (int i = 0; i < outputNodes.Count; i += 1)
		{
			UpdateBezier(outputLines[i], nodeOutput.Position, outputNodes[i].nodeInput.GlobalPosition - GlobalPosition);
		}
		
		if (inputNode != null)
			inputNode.UpdateBezier(inputNode.outputLines[inputNode.GetOutputToNode(this)], inputNode.nodeOutput.Position, nodeInput.GlobalPosition - inputNode.GlobalPosition);
	}

	void ToggleDescription()
	{
		descriptionParent.Visible = !descriptionParent.Visible;
		descEditArea.Monitoring = !descEditArea.Monitoring;
		descArea.Monitoring = !descArea.Monitoring;
		dropdown.Rotation += (float)Math.PI;
	}

	public void ConnectGraphNode(GraphNode inNode, bool newLine = false)
	{
		if (newLine)
		{
			currentOutputLine = lineScene.Instantiate() as Line2D;
			AddChild(currentOutputLine);
		}
		
		outputNodes.Add(inNode);
		outputLines.Add(currentOutputLine);
		
		inNode.ClearInput(this);
		UpdateBezier(currentOutputLine, nodeOutput.Position, inNode.nodeInput.GlobalPosition - GlobalPosition);
	}

	void ClearInput(GraphNode newInput)
	{
		inputNode?.ClearOutput(this);
		inputNode = newInput;
	}

	void ClearOutput(GraphNode fromNode)
	{
		int nodeIndex = GetOutputToNode(fromNode);
		
		if (nodeIndex != -1)
		{
			outputNodes[nodeIndex].inputNode = null;
			outputNodes.RemoveAt(nodeIndex);
			
			outputLines[nodeIndex].QueueFree();
			outputLines.RemoveAt(nodeIndex);
		}
	}

	int GetOutputToNode(GraphNode fromNode)
	{
		for (int i = 0; i < outputNodes.Count; i += 1)
		{
			if (outputNodes[i] == fromNode) return i;
		}

		return -1;
	}

	public void SetLabel(string newLabel)
	{
		mainLabel.Text = "[center]" + newLabel;
	}

	public void SetDescription(string newDesc)
	{
		descLabel.Text = newDesc;
	}

	void UpdateBezier(Line2D targetLine, Vector2 start, Vector2 end)
	{
		float targetControlLength = bezierControlLength;
		float distToMouse = start.DistanceTo(end);
		if (bezierControlLength > distToMouse) targetControlLength = distToMouse; 
		
		Vector2[] bezierPoints = new[] { start, 
			start + Vector2.Right * targetControlLength,  
			end + Vector2.Left * targetControlLength, 
			end};

		Vector2[] returnBezier = Bezier.GeneralBezier(bezierPoints, bezierSteps);
		targetLine.ClearPoints();
		foreach (Vector2 bezierPoint in returnBezier)
		{
			targetLine.AddPoint(bezierPoint);
		}
	}

	void CheckMouse(int controlIndex, bool set, Area2D area)
	{
		if (area.IsInGroup("MouseTracker")) interactBuffer[controlIndex] = set;
	}

	void EditLabel()
	{
		mainCamera.EditLabel(this, mainLabel.Text[8..]);
	}

	public string[] GetInfo()
	{
		return new[] { mainLabel.Text[8..], descLabel.Text };
	}

	void CheckScrollOverride(Area2D area, bool set)
	{
		if (!area.IsInGroup("MouseTracker")) return;

		mainCamera.overrideScrolling = set;
	}

	void MouseEnterOutput(Area2D area) => CheckMouse(0, true, area);
	void MouseExitOutput(Area2D area) => CheckMouse(0, false, area);
	void MouseEnterInput(Area2D area) => CheckMouse(1, true, area);
	void MouseExitInput(Area2D area) => CheckMouse(1, false, area);
	void MouseEnterHeader(Area2D area) => CheckMouse(2, true, area);
	void MouseExitHeader(Area2D area) => CheckMouse(2, false, area);
	void MouseEnterDropdown(Area2D area) => CheckMouse(3, true, area);
	void MouseExitDropdown(Area2D area) => CheckMouse(3, false, area);
	void MouseEnterDescEdit(Area2D area) => CheckMouse(4, true, area);
	void MouseExitDescEdit(Area2D area) => CheckMouse(4, false, area);
	void MouseEnterLabelEdit(Area2D area) => CheckMouse(5, true, area);
	void MouseExitLabelEdit(Area2D area) => CheckMouse(5, false, area);
	void MouseEnterDesc(Area2D area) => CheckScrollOverride(area, true);
	void MouseExitDesc(Area2D area) => CheckScrollOverride(area, false);
}
