using Godot;
using System;

public partial class Bezier : GodotObject
{
    public static Vector2[] GeneralBezier(Vector2[] controlPoints, int steps)
    {
        Vector2[] points = new Vector2[steps + 1];

        for (int i = 0; i < steps + 1; i += 1)
        {
            points[i] = ComputeBezier(controlPoints, i / (float)steps);
        }
        
        return points;
    }

    static Vector2 ComputeBezier(Vector2[] controlPoints, float t)
    {
        if (controlPoints.Length == 2) return controlPoints[0].Lerp(controlPoints[1], t); // Base case

        int cascadeLength = controlPoints.Length - 1;
        Vector2[] cascadePoints = new Vector2[cascadeLength];
        for (int i = 0; i < cascadeLength; i += 1)
        {
            cascadePoints[i] = controlPoints[i].Lerp(controlPoints[i + 1], t);
        }

        return ComputeBezier(cascadePoints, t);
    }
}
