using Godot;
using System;

public partial class CameraController : Camera2D
{
	[Export] float minZoom, maxZoom;
	[Export] float zoomAccel, zoomDeccel;
	[Export] float zoomShiftCoef;
	float zoomSpeed;

	[Export] Node2D mouseTracker;
	[Export] PackedScene graphNodeScene;

	[Export] LineEdit labelInput;
	
	[Export] TextEdit textEdit;

	public bool overrideDragging, overrideScrolling;
	bool disableDragging;
	public GraphNode targetInput;
	
	Vector2 prevMousePos;
	bool dragging;

	GraphNode editingNode;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{	
		if (!overrideScrolling) Zoom += Vector2.One * (float)(zoomSpeed * delta);
		zoomSpeed -= zoomSpeed * zoomDeccel * (float)delta;
		
		float clampedZoom = Math.Clamp(Zoom.X, minZoom, maxZoom);
		if (Math.Abs(Zoom.X - clampedZoom) > 0.01f)
		{
			Zoom = Vector2.One * clampedZoom;
			zoomSpeed = 0f;
		}

		if (!overrideScrolling) GlobalPosition += GetLocalMousePosition() * (zoomShiftCoef * zoomSpeed * (float)delta);

		mouseTracker.GlobalPosition = GetGlobalMousePosition();
		
		if (!dragging || overrideDragging || disableDragging) return;

		Vector2 mousePos = GetLocalMousePosition();
		Vector2 mouseDelta = mousePos - prevMousePos;

		Position -= mouseDelta;

		prevMousePos = mousePos;
	}

	public override void _Input(InputEvent @event)
	{
		if (@event.IsAction("mouse_down"))
		{
			dragging = @event.IsPressed();
			if (dragging)
			{
				prevMousePos = GetLocalMousePosition();
			}
		}
		else if (@event.IsAction("mouse_right_down") && @event.IsActionPressed("mouse_right_down")) SpawnGraphNode(GetGlobalMousePosition());
		else if (@event.IsAction("mouse_scroll_up") && @event.IsReleased())
		{
			zoomSpeed += zoomAccel;
		}
		else if (@event.IsAction("mouse_scroll_down") && @event.IsReleased())
		{
			zoomSpeed -= zoomAccel;
		}
	}

	public GraphNode SpawnGraphNode(Vector2 spawnPos, string newLabel = "Label", string newDesc = "")
	{
		GraphNode newGraphNode = graphNodeScene.Instantiate() as GraphNode;
		if (newGraphNode == null) return null;

		newGraphNode.GlobalPosition = spawnPos;
		newGraphNode.mainCamera = this;
		newGraphNode.SetLabel(newLabel);
		newGraphNode.SetDescription(newDesc);

		GetParent().AddChild(newGraphNode);

		return newGraphNode;
	}

	public void EditLabel(GraphNode fromNode, string prevLabel)
	{
		if (editingNode != null) return;
		
		labelInput.Text = prevLabel;
		labelInput.Call("toggle");
		labelInput.Editable = !labelInput.Editable;
		
		editingNode = fromNode;
		disableDragging = true;
	}
	
	void LabelSubmit(string inputString)
	{
		if (editingNode == null) return;
		
		labelInput.Clear();
		labelInput.Call("toggle");
		labelInput.Editable = false;
		
		editingNode.SetLabel(inputString);

		editingNode = null;
		disableDragging = false;
	}

	public void EditDescription(GraphNode fromNode, string prevDesc)
	{
		if (editingNode != null) return;
		
		textEdit.Text = prevDesc;
		textEdit.Call("toggle");

		editingNode = fromNode;
		disableDragging = true;
	}

	void FinishEditDesc()
	{
		if (editingNode == null) return;
		
		editingNode.SetDescription(textEdit.Text);
		textEdit.Clear();
		textEdit.Call("toggle");

		editingNode = null;
		disableDragging = false;
	}
}
