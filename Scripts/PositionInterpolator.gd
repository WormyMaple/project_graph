extends Control

@export var min_pos: Vector2
@export var max_pos: Vector2
@export var interp_curve: Curve
@export var interp_time_max: float
var interp_time = 0.0
var interp_dir := -1
var changing: bool

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !changing:
		return
		
	interp_time += delta * interp_dir;
	if interp_time < 0.0:
		interp_time = 0.0;
		changing = false;
	elif interp_time > interp_time_max:
		interp_time = interp_time_max
		changing = false;

	var sample = interp_curve.sample(interp_time / interp_time_max)	
	position = min_pos.lerp(max_pos, sample)
		
func toggle():
	changing = true;
	interp_dir = -interp_dir
